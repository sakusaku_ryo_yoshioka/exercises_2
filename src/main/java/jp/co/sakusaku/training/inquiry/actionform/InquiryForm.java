package jp.co.sakusaku.training.inquiry.actionform;

import org.apache.struts.validator.ValidatorForm;

public class InquiryForm extends ValidatorForm {


	private String title;
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


}
